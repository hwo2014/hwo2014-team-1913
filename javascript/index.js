/*

 // Notes
 // Keimola: kaistanvaihdosta saa lyhennettyä aikaa?
 // Germany: jyrkät radius 50 mutkat aiheuttaa ongelmia
 // 		mennään ulos paloissa #3, 8, 20, 24, 30, 38, 47
			- auton perä myös taitaa lähteä vauhdikkaammin liikkeelle?


 // TODO: calculate how much car has moved?
 // (TODO: is curve ahead?)
 // TODO: can we go full throttle?
 // TODO: can we change lane and when?
	// TODO: vaihtopalan etsintä olisi hyvä tehdä, kun edellisen vaihtopalan puolivälin (vaihdon) yli on päästy

 // LATER TODO: is opponent ahead or behind?
 // LATER TODO: can we block opponent?
 // LATER TODO: are we faster than opponent?
 // LATER TODO: does opponent crash at all?


//todo DONE lanen vaihdot! laskee montako vasenta ja oikeeta mutkaa on ennen seuraavaa vaihtoa ja vaihtaa sen mukaan!
//next todo auto ei välttämättä ehdi jarruttaa tarpeeksi ennen jyrkkää mutkaa keimolan alussa? (pitkä suora)
//täytyy koodata systeemi joka katsoo eron tavoitenopeuden ja nykyisen nopeuden välillä ja
//alkaa jarruttaa jo aikaisemmin!



//todo kattoa paljonko auton vauhti kiihtyy maksimissaan per tick.. ---vaikuttaa sekavalta?

//todo   DONE  kaikkien palojen tarkka pituus, myös mutkien.. we have to use math here!
//todo muutetaan radan luku tulevista paloista matkapohjauseksi ???? harkitaan
//esim katsotaan kuinka monen pikselin päässä tulee mutka..
//nykyään lukee vain jos mutka on kahden palan säteellä, voisi olla tarkempi..

//todo pitää laskea kuinka paljon auto kallistuu per tick kulmassa
//pitää ajella ekaan mutkaan eri nopeuksilla ja tehdä tilastoa...
//kun alkaa olla perusidea kasassa voidaan tehdä koodi joka
//laskee voiko auton nykyisellä nopeudella selvitä seuraavasta mutkasta
//jos voi ja jää varaa yli > nostetaan nopeutta
//jos ei > lasketaan nopeutta

//later todo s-mutkissa huomioitava että auto on jo jonkin verran kulmassa eri suuntaan siihen
//tullessa joten nopeutta voi olla enemmän kuin normaalisti

//infoa sivulla
 If you drive too fast into a bend, you'll crash. There's only a
 certain amount of friction in your wheels, after all. So you
 need to slow down a bit in the corners. How much, that's for you
 to find out. And when you crash, you'll be off the track for N seconds (typically 5), so you
 don't want to be crashing in the race. You may use the
 qualifying session to find your limits before the race. And
 prepare for different tracks, different levels of friction and
 engine power too!


 */



var net = require("net");
var JSONStream = require('JSONStream');

var serverHost = process.argv[2];
var serverPort = process.argv[3];
var botName = process.argv[4];
var botKey = process.argv[5];

console.log("I'm", botName, "and connect to", serverHost + ":" + serverPort);

// init default values
var defaultThrottle = 0.6565; // trying to find nice default speed (0.656 for keimola)
var throttle = defaultThrottle;
var throttleReduction = 0.001; // how much throttle reduced after crash
var gameInit; // contains track pieces and other track info
var trackLengthInPieces = 0;
var trackLength = 0;
var trackCurves = 0;
var lanes = 0;
var cars = 0;
var crashes = 0;

var turboAvailable = false; // turbo käytettävissä
var turboChances = 0; // turboja saatu

//tilannenopeudet
//kommentoituna optiimit ilman lanenvaihtoja
var speed_straight = 10; //10

var speed_45_short_outerlane = 7.45; //7.45 vielä menee
var speed_45_outerlane = 7.1; //7.1 toimiva

var speed_45_short_innerlane = 6.71; //6.83
var speed_45_innerlane = 6.45; //6.56

var speed_22corner = 10; //7.5 toimiva //8.5 oli vielä ajettava ainakin keimolassa
var speed_Scorner = 9; //8 tai 10 käy ihan hyvin, ei kuitenkaan ehdi kaasuttaa niin paljon..

// Mutkataulukko tilannenopeuksilla (nopeudet lasketaan erikseen?)
//		Mutkan jälkeen voi olla yhtä paljon samanlaisia paloja toiseen suuntaan, jolloin kyseessä on S-mutka
//		Otetaan S-mutkassa huomioon välissä olevia suorapaloja? germany-radassa on lyhyt suora pala S-mutkien välissä
//		Voidaanko mutkaan tulla hieman kovempaa ja hidastaa mutkaan tultaessa sen mukaan, kuinka pitkä mutka on?

//		Kannattaako kaistanopeuksia merkitä taulukkoon, jos niitä voi radasta riippuen olla jopa 4?
var curveSpeeds = {"mutkat": [
        
		{ "totalAngle": 22.5,	"radius": 50, "lanes": [ { "targetSpeed": 5 }, { "targetSpeed": 5 } ] },
        { "totalAngle": 45,		"radius": 50, "lanes": [ { "targetSpeed": 5 }, { "targetSpeed": 5 } ] },
        { "totalAngle": 90,		"radius": 50, "lanes": [ { "targetSpeed": 5 }, { "targetSpeed": 5 } ] },
        { "totalAngle": 135,	"radius": 50, "lanes": [ { "targetSpeed": 5 }, { "targetSpeed": 5 } ] },
        { "totalAngle": 180,	"radius": 50, "lanes": [ { "targetSpeed": 5 }, { "targetSpeed": 5 } ] },
		
		{ "totalAngle": 22.5,	"radius": 100, "lanes": [ { "targetSpeed": 5 }, { "targetSpeed": 5 } ] },
        { "totalAngle": 45,		"radius": 100, "lanes": [ { "targetSpeed": 5 }, { "targetSpeed": 5 } ] },
        { "totalAngle": 90,		"radius": 100, "lanes": [ { "targetSpeed": 5 }, { "targetSpeed": 5 } ] },
        { "totalAngle": 135,	"radius": 100, "lanes": [ { "targetSpeed": 5 }, { "targetSpeed": 5 } ] },
        { "totalAngle": 180,	"radius": 100, "lanes": [ { "targetSpeed": 5 }, { "targetSpeed": 5 } ] },
        
		{ "totalAngle": 22.5,	"radius": 200, "lanes": [ { "targetSpeed": 10 }, { "targetSpeed": 10 } ] },
        { "totalAngle": 45,		"radius": 200, "lanes": [ { "targetSpeed": 5 }, { "targetSpeed": 5 } ] },
        { "totalAngle": 90,		"radius": 200, "lanes": [ { "targetSpeed": 5 }, { "targetSpeed": 5 } ] },
        { "totalAngle": 135,	"radius": 200, "lanes": [ { "targetSpeed": 5 }, { "targetSpeed": 5 } ] },
        { "totalAngle": 180,	"radius": 200, "lanes": [ { "targetSpeed": 5 }, { "targetSpeed": 5 } ] },

        { "totalAngle": 0,		"radius": 0, "lanes": [ { "targetSpeed": 10 }, { "targetSpeed": 10 } ] }
    ]
};

//piece jossa katsotaan nopeus kuntoon seuraavan kerran alussa pitää olla 0
var nextSpeedCheck = 0;

//piece jossa seuraava kaistanvaihto
var nextSwitchPiece = 1;

//kulma jossa auto lentää mehtään
var failsafe = false; //yrittää pysäyttää auton jos liian lähellä radalta tippumista, hidastaa aikaa hiukan..
//var carCrashAngle = 60; //60 villi veikkaus tällä hetkellä?
var failsafeAngle = 58; // kulma jossa failsafe käynnistyy (auto crashaa 60:ssä?)

var ballsyMode = false; //antifailsafe, kiihdyttelee jos näyttää siltä että mutkassa on varaa ajaa kovempaa
//ei toimi vielä kunnolla.. TODO tutkia jos autolla on negatiivinen kulma tullessa mutkaan

//yleis muuttujia joita tarvitaan koodissa, ei saa muuttaa
var speedBeforeFailsafe = 0;
var targetSpeed;
var speedInPreviousTick = 0;
var slowdownFactor = 0.01;
var carAngle;
var currentLane;

var pieces = [];
var _lanes = [];
var _cars = [];
var _raceSession = [];

var oldPosInPiece = 0;
var oldPieceIndex = 0;
var oldCarAngle = 0;

var laneSwitchSended = false;



// advanced join
/*
client = net.connect(serverPort, serverHost, function() {
  return send({
    msgType: "createRace",
    data: {
	  botId: {
	    name: botName,
        key: botKey
	  },
      trackName: "usa", //keimola, germany, usa
	  carCount: 1
    }
  });
});
*/

// default join, TÄTÄ PITÄÄ KÄYTTÄÄ JOTTA CI-TESTI MENEE LÄPI
client = net.connect(serverPort, serverHost, function() {
  return send({
    msgType: "join",
    data: {
      name: botName,
      key: botKey
    }
  });
});


function send(json) {
  client.write(JSON.stringify(json));
  return client.write('\n');
};



//mutkan laskufunktio OLD
function countCurves(p, start) {
    var curveAngle = p[start].angle;
    var curves = 0;
    //lasketaan kuinka monta palaa on mutkaa samaan suuntaan
    for (var n = start; n < p.length; n++) {
        if (p[n].type == "jyrkka" && p[n].angle == curveAngle ) {
            curves++;
        } else {
            break;
        }
    }
    return curves;
};



//mutkan laskufunktio
function countTotalAngle(_p, _start) {
    var _radius = _p[_start].radius;
    var _totalAngle = 0;
    //lasketaan anglea niin kauan kun sama radius jatkuu
    for (var n = _start; n < _p.length; n++) {
        if (_p[n].radius == _radius ) {
            _totalAngle += _p[n].angle;
        } else {
            break;
            //return _totalAngle;
        }
    }

    var data = {
        totalAngle: _totalAngle,
        curveEnd: n - 1
    };

    return data;
};

jsonStream = client.pipe(JSONStream.parse());

jsonStream.on('data', function(data) {
  if (data.msgType === 'carPositions') {

      //otetaan tarvittavat tiedot tickiltä
      carAngle = data.data[0].angle;
      if (carAngle < 0) carAngle = -carAngle;

      //trackpiece
      var pos = data.data[0].piecePosition.pieceIndex;

      //tämänhetkinen lane
      var lane = data.data[0].piecePosition.lane.startLaneIndex;

      //lasketaan nopeus
      var speed;
      if (pos == oldPieceIndex) {
          speed = data.data[0].piecePosition.inPieceDistance - oldPosInPiece;
      } else {
          //korjaus nopeuslaskentaan palan vaihdoissa
          speed = pieces[oldPieceIndex].lengths[lane] - oldPosInPiece + data.data[0].piecePosition.inPieceDistance;
          //console.log(pieces[oldPieceIndex].lengths[lane] + "-" + oldPosInPiece + " korjattu nopeus> " + speed);
      }

        //katotaan miten auton vauhti muuttuu per tick
        var speedChangeInTick = speed - speedInPreviousTick;
       // console.log("vauhti kiihtyi> " + (speed - speedInPreviousTick));
      //  console.log(1 - speed / speedChangeInTick);

        speedInPreviousTick = speed;

        var nextPos;
        if (pos < pieces.length-1) {
            nextPos = pos +1
        } else {
            nextPos = 0;
        }

      //console.log(pieces[pos].angle);

      /*
        noniin uusin systeemi perustuu
        speedcheckiin

        aina kun nopeutta muutetaan laitetaan
        muistiin seuraava piece jossa nopeutta tarvitsee muuttaa

        TODO jarruttaa liikaa S-mutkassa?

       */
      var p1 = pieces[pos];
      var p2 = pieces[nextPos];
      if (pos == nextSpeedCheck) {

          if (p1.type == "suora" && p2.type == "suora") {
             // console.log("suora!!!!!!!!!!!!!!!");
              targetSpeed = speed_straight;
              nextSpeedCheck = pos +1;
          }

          //jyrkät mutkat
          //if (p1.type == "suora" && p2.type == "jyrkka") {
          if (p2.type == "jyrkka") {

              //lasketaan mutkan pituus
            //  var curves = countCurves(pieces, nextPos);
              var curveData = countTotalAngle(pieces, nextPos);

             // console.log((pos + curves) + " -*********- " + curveData.curveEnd);
              nextSpeedCheck = curveData.curveEnd-1;
             // if (pieces[pos + curves].angle == -p1.angle) nextSpeedCheck++;

              if (curveData.totalAngle == 90) {
                  if (p2.angle > 0) { //positiivinen angle..
                      if (data.data[0].piecePosition.lane.endLaneIndex == 0) {
                            targetSpeed = speed_45_short_outerlane;
                      } else {
                            targetSpeed = speed_45_short_innerlane;
                      }

                  } else {
                      if (data.data[0].piecePosition.lane.endLaneIndex == 0) {
                            targetSpeed = speed_45_short_innerlane;
                      } else {
                            targetSpeed = speed_45_short_outerlane;
                      }
                  }

              } else {

                  if (p2.angle > 0 && data.data[0].piecePosition.lane.endLaneIndex == 0) { //positiivinen angle..
                      targetSpeed = speed_45_outerlane;
                  } else {
                      targetSpeed = speed_45_innerlane;
                  }
              }

              //S-mutka
              /*
              if (p1.angle == -p2.angle) {
                  targetSpeed = speed_Scorner;
                  console.log("S-mutka detected! hanaaa");
                  nextSpeedCheck = pos +1;
              }*/

          }

          //loivat (näitä pitää vielä säätää..)
          if (p1.type == "suora" && p2.type == "loiva") {
              targetSpeed = speed_22corner;
              nextSpeedCheck = pos +1;
          }
          if (p1.type == "loiva" && p2.type == "loiva") {
              targetSpeed = speed_22corner;
              nextSpeedCheck = pos +1;
          }
          if (p1.type == "loiva" && p2.type == "suora") {
              targetSpeed = speed_22corner;
              nextSpeedCheck = pos +1;
          }

          if (nextSpeedCheck == pos) nextSpeedCheck++;
          if (nextSpeedCheck > pieces.length -1) nextSpeedCheck = 0;

      }

      var laneLenghts = [];

	  // seuraavan vaihtopala etsintä
      if (nextPos == nextSwitchPiece) {
		  var s1 = nextPos;
		  var i = 1; // 1 jotta ei haeta samaa palaa
		  var foundIt = false;
		  //while (i < trackLengthInPieces || foundIt == 0) {

          laneLenghts[0] = 0;
          laneLenghts[1] = 0;

		  while (!foundIt) {

            //lasketaan lanejen pituudet
            laneLenghts[0] += pieces[s1+i].lengths[0];
            laneLenghts[1] += pieces[s1+i].lengths[1];

			if (pieces[s1+i].switch === true) {
				nextSwitchPiece = s1+i; // merkitään seuraava vaihtopala
				foundIt = true;
			}
			i++;
		    //rata loppuu, palataan alkuun
			if (s1+i >= trackLengthInPieces) {
				s1 = 0;
			}
		  }

          console.log(laneLenghts[0] + " -- " + laneLenghts[1]);

          //katsoo lyhyymmän lanen ja vaihtaa sinne..
          var shortestLane;
          if (laneLenghts[0] < laneLenghts[1]) {
              shortestLane = 0;
          } else {
              shortestLane = 1;
          }
          //jos saman mittaset lanet, turha vaihtaa...
          if (laneLenghts[0] == laneLenghts[1]) laneSwitchSended = true;
          if (!laneSwitchSended) {
              if (data.data[0].piecePosition.lane.startLaneIndex != shortestLane) {

                  laneSwitchSended = true;

                  console.log("---- CAR SWITCHED LANE! (from lane #" + data.data[0].piecePosition.lane.startLaneIndex+ " -----");

                  if (data.data[0].piecePosition.lane.startLaneIndex == 0) {
                      send({"msgType": "switchLane", "data": "Right"});
                  } else {
                      send({"msgType": "switchLane", "data": "Left"});
                  }
              }
          }
	  }
	  
	  
      //jos mutkasta alkaa jo selviytyä, kaasua voi vähän nostaa
      //NOTE tämä toimii niin jos auto on jo suoristumassa mutkan loppuosassa
      //nopeutta nostetaan.
      //S-mutkat aiheuttavat logiikkaongelman, sillä mutkan keskellä auto on vasta palautumassa vastakkaisesta
      //kulmasta, eli tämä luulee että auto on suoristumassa!
      //KORJATTU sillä että tätä ei tehdä jos edellinen pala on mutka vastakkaiseen suuntaan!


      if (ballsyMode) {
          if (p1.type == "jyrkka" && pieces[pos-1].angle != -p1.angle) {
              if (carAngle < oldCarAngle - 4) {
                  console.log("nayttaa niin hyvalta tama mutka etta lisaa vauhtia");
                  console.log(oldCarAngle + "-" + carAngle + "=" + (oldCarAngle - carAngle));
                  targetSpeed = 10;
              }
          }
      }

        //failsafe jos meinaa lentää
        if (carAngle > failsafeAngle && failsafe) {
            console.log("too much speed.. Activate failsafe");

            if (targetSpeed != 0) speedBeforeFailsafe = targetSpeed;
            targetSpeed = 0;
        } else {
            if (speedBeforeFailsafe != 0) {
                targetSpeed = speedBeforeFailsafe;
                speedBeforeFailsafe = 0;
            }
        }

        //throttle määriytyy tarvittavan nopeuden muutoksen mukaan
        if (speed > targetSpeed - speedChangeInTick *0.66 && speed < targetSpeed - speedChangeInTick * 0.66) {

              //tarpeeksi lähellä kohdetta, tasainen throttle
              send({
                  msgType: "throttle",
                  data: targetSpeed
              });

        } else {
            //kaukana kohteesta throttle on aina maksimi tai minimi
            if (speed > targetSpeed) {

                send({
                    msgType: "throttle",
                    data: 0
                });

            } else {

                send({
                    msgType: "throttle",
                    data: 1
                });

            }
        }

	// print time every 0.25 second
	//if (data.gameTick % (15 * 1) == 0) {
      if (pos != oldPieceIndex) {

        laneSwitchSended = false;

		currentLane = data.data[0].piecePosition.lane.endLaneIndex;
		
        console.log('---------');
		console.log((data.gameTick/60) + ' seconds has passed. (Ticks: ' + data.gameTick + ')');
        console.log('Car is on piece #' + pos + '/' + trackLengthInPieces + ' Lap: ' + (data.data[0].piecePosition.lap+1) + '/' + gameInit.data.race.raceSession.laps);
        console.log('Current speed is ' + speed + ' Target speed is ' + targetSpeed);
		console.log('Current lane: ' + (currentLane+1) + '/' + lanes + ' (' + currentLane + ')');
        console.log('Next speed change is on piece #' + nextSpeedCheck);
        console.log('Next switch is on piece #' + nextSwitchPiece);
        //console.log("")
        console.log('Next piece: #' + nextPos + ' Palan kulma> ' + pieces[nextPos].angle + ' radius> ' + pieces[nextPos].radius);
		// onko palassa vaihtaja?
		if (pieces[nextPos].switch === true) {
			console.log('Switch: Yes');
		}
		else {
			console.log('Switch: No');
		}
        console.log('---------');



          /*
        if (pieces[pos].angle == 0) {
            console.log('SUORAAA, TALLAPOHJAAN');
        } else {
            console.log('MUTKA TULOSSA -- ' + pieces[nextPos].angle +'!!');
        }*/

	}

    //tämä tickin tietoja muistiin seuraavaa tickiä varten
    oldPosInPiece = data.data[0].piecePosition.inPieceDistance;
    oldPieceIndex = pos;
    oldCarAngle = carAngle;




  } else {
    if (data.msgType === 'join') {
      console.log('Joined')
    } else if (data.msgType === 'gameStart') {
      console.log('Race started');
	  console.log('Default throttle ' + throttle);
    } else if (data.msgType === 'gameEnd') {
      console.log('Race ended');
    }
	else if (data.msgType === 'gameInit')
	{
	  gameInit = data; // save track information for later use, in case it's needed
	  pieces = gameInit.data.race.track.pieces;
	  _lanes = gameInit.data.race.track.lanes;
	  _cars = gameInit.data.race.cars;
	  _raceSession = gameInit.data.race.raceSession;
	  crashes = 0;
	  
	  // TRACK DETAILS
      console.log('');
      console.log('-------------------------------');
	  
	  // general information on track
      console.log('Game init');
	  
	  // track pieces
      console.log('Track pieces:');
	  trackLengthInPieces = 0;
	  trackLength = 0;
	  trackCurves = 0;
	  for (var key in pieces) {
		var value = pieces[key];
		trackLengthInPieces++;
		//console.log(trackLengthInPieces + ': ' + trackPiece(value) + ' '); // print
		// straight
		if (pieces[key].length) {
			trackLength = trackLength + pieces[key].length;
		}
		// curve
		if (pieces[key].radius) {
			trackCurves++;
			// TODO: how to calculate curve length?
		}
	  }
	  // TODO: total length doesn't contain curves!
	  console.log('Track name is ' + gameInit.data.race.track.name);
	  console.log('Total pieces: ' + trackLengthInPieces + ' Total Curve pieces: ' + trackCurves + ' Total length (straight only): ' + trackLength + '');
	  console.log('');
	  
	  // lanes
	  lanes = 0;
	  for (var key in _lanes) {
		var value = _lanes[key];
		lanes++;
		console.log('Lane ' + lanes + ' distanceFromCenter ' + _lanes[key].distanceFromCenter + ' ');
	  }
	  console.log('Total lanes: ' + lanes + '');
	  
	  // cars
	  cars = 0;
	  for (var key in _cars) {
		var value = _cars[key];
		cars++;
		console.log('Car ' + cars + ' Name: ' + _cars[key].id.name + ' Color: ' + _cars[key].id.color + ' ');
	  }
	  console.log('Total cars: ' + cars + '');
	  
	  // race session
	  console.log('Laps: ' + gameInit.data.race.raceSession.laps + ' MaxTime: ' + gameInit.data.race.raceSession.maxLapTimeMs + ' quickRace: ' + gameInit.data.race.raceSession.quickRace);
	  
      console.log('-------------------------------');
      console.log('');

      //pieces = data.data.race.track.pieces; // määritellään jo gameInitin alussa

        //korjataan undefinet nolliksi..
      for (var n = 0; n < pieces.length; n++) {
          if (pieces[n].angle == undefined) {
              pieces[n].angle = 0;
          }
		  
		  // merkitään undefined switchit falseksi
          if (pieces[n].switch == undefined) {
              pieces[n].switch = false;
          }

          //merkitään undefined radius 0:ksi (suorat..)
          if (pieces[n].radius == undefined) {
              pieces[n].radius = 0;
          }

          //katsotaan kaikille paloille pituudet
          //urgh niin tottakai
          //kulmat on eri pituisia sisä ja ulkolaneilla
          //tehdään taulukko pituuksista eri laneille...
          pieces[n].lengths = [];
          for (var lane in _lanes) {
              if (pieces[n].angle == 0) {
                  pieces[n].lengths[lane] = pieces[n].length;
              } else {
                  var a = pieces[n].angle;
                  var len;
                  if (a < 0) {
                      a = -a;
                      len = (pieces[n].radius + _lanes[lane].distanceFromCenter) * 2 * Math.PI / 360 * a;
                  } else {
                      len = (pieces[n].radius - _lanes[lane].distanceFromCenter) * 2 * Math.PI / 360 * a;
                  }
                  pieces[n].lengths[lane] = len;
              }

              //määritetään paloille tyyppi - helpottaa if lausesoppaa kun tutkitaa sopivaa nopeutta
              switch (pieces[n].radius) {

                  case 0:
                      pieces[n].type = "suora";
                      break;
                  case 200:
                      pieces[n].type = "loiva";
                      break;
                  case 100:
                      pieces[n].type = "jyrkka";
                      break;
                  case 50:
                      pieces[n].type = "tosijyrkka";
                      break;

              }

          }

          //printataan lane 0:n pituudet..
          console.log("#" + n + " pituus>" + pieces[n].lengths[0] + "   kulma>" + pieces[n].angle  + "  radius>" + pieces[n].radius + " switch>" + pieces[n].switch);

          //määritetään täällä nopeudet joka piecelle..
          //tarvitsee eri nopeudet joka lanelle!
          for (var i = 0; i < pieces.length; i++) {
          }

      }

      //console.log('tammonen rata ' + data.data.race.track.pieces);

    }
	else if (data.msgType === 'yourCar') {
      console.log('Your car \'' + data.data.name + '\' is ' + data.data.color + '.');
    }
	else if (data.msgType === 'lapFinished') {
	  var racer = data.data.car.name;
	  var color = data.data.car.color;
	  var lap = data.data.lapTime.lap + 1;
	  var laptime = data.data.lapTime.millis/1000; // lap time in seconds
      console.log(racer + '(' + color + ') finished lap ' + lap + ' in ' + laptime + ' seconds!');
	  console.log('You crashed ' + crashes + ' times...');
      console.log('Turbo: ' + turboAvailable);
      console.log('You had turbo chance ' + turboChances + ' times...');
    } 
	else if (data.msgType === 'crash') {

    /*  console.log(data.data.name + ' (' + data.data.color + ') has crashed!');

      console.log(data.data.name + ' (' + data.data.color + ') has crashed! (GameTick ' + data.gameTick + ')');
>	  // lower throttle by 0.001
	  throttle = throttle - throttleReduction;
      console.log('Throttle lowered by ' + throttleReduction + '. Current throttle ' + throttle);*/
      console.log('--*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*--------------');
      console.log('BOOM BIUM KABOOMMMM (car crashed..) angle was ' + carAngle );
      console.log(' speed was ' +speedInPreviousTick);
      console.log('----**-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*----------');
	  crashes++;
    } 
	else if (data.msgType === 'spawn') {
      console.log(data.data.name + ' (' + data.data.color + ') has spawned! (GameTick ' + data.gameTick + ')');
    } 
	else if (data.msgType === 'finish') {
      console.log(data.data.name + ' (' + data.data.color + ') finished the ' + gameInit.data.race.track.name + ' Race!');
      console.log('You crashed ' + crashes + ' times...');
      console.log('You had turbo chance ' + turboChances + ' times...');
	}
	else if (data.msgType === 'dnf') {
      console.log('DNF (did not finish): car \'' + data.data.car.name + '\' is ' + data.data.car.color + '.');
	  console.log('Reason: ' + data.data.reason);
    }
	else if (data.msgType === 'tournamentEnd') {
      console.log('Tournament ended');
    }
	else if (data.msgType === 'turboAvailable') {
      console.log('Turbo available!');
	  turboAvailable = true;
	  turboChances++;
    }
	
    send({
      msgType: "ping",
      data: {}
    });
  }
});

jsonStream.on('error', function() {
  return console.log("disconnected");
});

function ticksToSeconds(ticks) {
	return ticks/60;
}

function trackPiece(piece) {
	var string = '';
	
	if (piece.length) {
		string += 'length ' + piece.length + ' ';
	}
	if (piece.switch) {
		string += 'switch ' + piece.switch + ' ';
	}
	if (piece.radius) {
		string += 'radius ' + piece.radius + ' ';
	}
	if (piece.angle) {
		string += 'angle ' + piece.angle + ' ';
	}
	return string;
}