/*





 // TODO: calculate how much car has moved?
 // TODO: is curve ahead?
 // TODO: can we go full throttle?
 // TODO: can we change lane and when?

 // LATER TODO: is opponent ahead or behind?
 // LATER TODO: can we block opponent?
 // LATER TODO: are we faster than opponent?
 // LATER TODO: does opponent crash at all?


 //TODO MUUMI teen tämän seuraavaksi

 //todo kattoa paljonko auton vauhti kiihtyy maksimissaan per tick..

 //todo kaikkien palojen tarkka pituus, myös mutkien.. we have to use math here!
 //todo muutetaan radan luku tulevista paloista matkapohjauseksi
 //esim katsotaan kuinka monen pikselin päässä tulee mutka..
 //nykyään lukee vain jos mutka on kahden palan säteellä, voisi olla tarkempi..

 //todo pitää laskea kuinka paljon auto kallistuu per tick kulmassa
 //pitää ajella ekaan mutkaan eri nopeuksilla ja tehdä tilastoa...
 //kun alkaa olla perusidea kasassa voidaan tehdä koodi joka
 //laskee voiko auton nykyisellä nopeudella selvitä seuraavasta mutkasta
 //jos voi ja jää varaa yli > nostetaan nopeutta
 //jos ei > lasketaan nopeutta

 //later todo s-mutkissa huomioitava että auto on jo jonkin verran kulmassa eri suuntaan siihen
 //tullessa joten nopeutta voi olla enemmän kuin normaalisti

 //infoa sivulla
 If you drive too fast into a bend, you'll crash. There's only a
 certain amount of friction in your wheels, after all. So you
 need to slow down a bit in the corners. How much, that's for you
 to find out. And when you crash, you'll be off the track for N seconds (typically 5), so you
 don't want to be crashing in the race. You may use the
 qualifying session to find your limits before the race. And
 prepare for different tracks, different levels of friction and
 engine power too!


 */



var net = require("net");
var JSONStream = require('JSONStream');

var serverHost = process.argv[2];
var serverPort = process.argv[3];
var botName = process.argv[4];
var botKey = process.argv[5];

console.log("I'm", botName, "and connect to", serverHost + ":" + serverPort);

// init default values
var defaultThrottle = 0.6565; // trying to find nice default speed (0.656 for keimola)
var throttle = defaultThrottle;
var throttleReduction = 0.001; // how much throttle reduced after crash
var gameInit; // contains track pieces and other track info
var trackLengthInPieces = 0;
var trackLength = 0;
var trackCurves = 0;
var lanes = 0;
var cars = 0;

//tilannenopeudet
var speed_straight = 10;
var speed_double45corner = 6.56;
var speed_single45corner = 7.265; //7.2 ajettava ainakin keimolassa //7.265 lentää radalta eka kierroksella mutta saa kovan ajan toisella..
var speed_22corner = 9.2; //7.5 toimiva //8.5 oli vielä ajettava ainakin keimolassa
var speed_Scorner = 8.0; //työn alla


//kulma jossa auto lentää mehtään
var carCrashAngle = 59; //60 villi veikkaus tällä hetkellä?
var failsafe = true; //yrittää pysäyttää auton jos liian lähellä radalta tippumista, hidastaa aikaa hiukan..

var targetSpeed;

var speedInPreviousTick = 0;

var slowdownFactor = 0.01;

var carAngle;

var pieces = [];

var oldPosInPiece = 0;
var oldPieceIndex = 0;

client = net.connect(serverPort, serverHost, function() {
    return send({
        msgType: "join",
        data: {
            name: botName,
            key: botKey
        }
    });
});

function send(json) {
    client.write(JSON.stringify(json));
    return client.write('\n');
};

jsonStream = client.pipe(JSONStream.parse());

jsonStream.on('data', function(data) {
    if (data.msgType === 'carPositions') {

        targetSpeed = 6.98;

        carAngle = data.data[0].angle;
        //if (carAngle < 0) carAngle = -carAngle;

        var pos = data.data[0].piecePosition.pieceIndex;
        console.log("#" + pos + " > " + carAngle);


        //tämänhetkinen lane
        var lane = data.data[0].piecePosition.lane.startLaneIndex;

        //lasketaan nopeus
        var speed;
        if (pos == oldPieceIndex) {
            speed = data.data[0].piecePosition.inPieceDistance - oldPosInPiece;
        } else {
            //korjaus nopeuslaskentaan palan vaihdoissa
            speed = pieces[oldPieceIndex].lengths[lane] - oldPosInPiece + data.data[0].piecePosition.inPieceDistance;
            //console.log(pieces[oldPieceIndex].lengths[lane] + "-" + oldPosInPiece + " korjattu nopeus> " + speed);

            console.log(("pala vaihtui #" + pos + "  angle " + pieces[pos].angle))

        }
        oldPosInPiece = data.data[0].piecePosition.inPieceDistance;
        oldPieceIndex = pos;

        var speedChangeInTick = speed - speedInPreviousTick;
        // console.log("vauhti kiihtyi> " + (speed - speedInPreviousTick));
        //  console.log(1 - speed / speedChangeInTick);

        speedInPreviousTick = speed;

        var nextPos;
        if (pos < pieces.length-1) {
            nextPos = pos +1
        } else {
            nextPos = 0;
        }

        //failsafe jos meinaa lentää
        if (carAngle > carCrashAngle -1 && failsafe) {
            console.log("too much speed.. Activate failsafe");
            targetSpeed = 0;
        }

        //throttle määriytyy tarvittavan nopeuden muutoksen mukaan
        if (speed > targetSpeed - speedChangeInTick && speed < targetSpeed - speedChangeInTick) {

            //tarpeeksi lähellä kohdetta, tasainen throttle
            send({
                msgType: "throttle",
                data: targetSpeed
            });

        } else {
            //kaukana kohteesta throttle on aina maksimi tai minimi
            if (speed > targetSpeed) {

                send({
                    msgType: "throttle",
                    data: 0
                });

            } else {

                send({
                    msgType: "throttle",
                    data: 1
                });

            }
        }

    } else {
        if (data.msgType === 'join') {
            console.log('Joined')
        } else if (data.msgType === 'gameStart') {
            console.log('Race started');
            console.log('Default throttle ' + throttle);
        } else if (data.msgType === 'gameEnd') {
            console.log('Race ended');
        }
        else if (data.msgType === 'gameInit')
        {
            gameInit = data; // save track information for later use
            var _pieces = gameInit.data.race.track.pieces;
            var _lanes = gameInit.data.race.track.lanes;
            var _cars = gameInit.data.race.cars;
            var _raceSession = gameInit.data.race.raceSession;

            // TRACK DETAILS
            console.log('');
            console.log('-------------------------------');

            // general information on track
            console.log('Game init');

            // track pieces
            console.log('Track pieces:');
            trackLengthInPieces = 0;
            trackLength = 0;
            trackCurves = 0;
            for (var key in _pieces) {
                var value = _pieces[key];
                trackLengthInPieces++;
                //console.log(trackLengthInPieces + ': ' + trackPiece(value) + ' '); // print
                // straight
                if (_pieces[key].length) {
                    trackLength = trackLength + _pieces[key].length;
                }
                // curve
                if (_pieces[key].radius) {
                    trackCurves++;
                    // TODO: how to calculate curve length?
                }
            }
            // total length doesn't contain curves!
            console.log('Track name is ' + gameInit.data.race.track.name);
            console.log('Total pieces: ' + trackLengthInPieces + ' Total Curve pieces: ' + trackCurves + ' Total length (straight only): ' + trackLength + '');
            console.log('');

            // lanes
            lanes = 0;
            for (var key in _lanes) {
                var value = _lanes[key];
                lanes++;
                console.log('Lane ' + lanes + ' distanceFromCenter ' + _lanes[key].distanceFromCenter + ' ');
            }
            console.log('Total lanes: ' + lanes + '');

            // cars
            cars = 0;
            for (var key in _cars) {
                var value = _cars[key];
                cars++;
                console.log('Car ' + cars + ' Name: ' + _cars[key].id.name + ' Color: ' + _cars[key].id.color + ' ');
            }
            console.log('Total cars: ' + cars + '');

            // race session
            console.log('Laps: ' + gameInit.data.race.raceSession.laps + ' MaxTime: ' + gameInit.data.race.raceSession.maxLapTimeMs + ' quickRace: ' + gameInit.data.race.raceSession.quickRace);

            console.log('-------------------------------');
            console.log('');

            pieces = data.data.race.track.pieces;

            //korjataan undefinet nolliksi..
            for (var n = 0; n < pieces.length; n++) {
                if (pieces[n].angle == undefined) {
                    pieces[n].angle = 0;
                }

                //katsotaan kaikille paloille pituudet
                //urgh niin tottakai
                //kulmat on eri pituisia sisä ja ulkolaneilla
                //tehdään taulukko pituuksista eri laneille...
                pieces[n].lengths = [];
                for (var lane in _lanes) {
                    if (pieces[n].angle == 0) {
                        pieces[n].lengths[lane] = pieces[n].length;
                    } else {
                        var len = (pieces[n].radius - _lanes[lane].distanceFromCenter) * 2 * Math.PI / 360 * pieces[n].angle;
                        pieces[n].lengths[lane] = len;
                    }

                }

                //printataan lane 0:n pituudet..
                console.log("#" + n + " pituus>" + pieces[n].lengths[0] + "   kulma>" + pieces[n].angle  + "  radius>" + pieces[n].radius);

            }

            //console.log('tammonen rata ' + data.data.race.track.pieces);

        }
        else if (data.msgType === 'yourCar') {
            console.log('Your car \'' + data.data.name + '\' is ' + data.data.color + '.');
        }
        else if (data.msgType === 'lapFinished') {
            var racer = data.data.car.name;
            var color = data.data.car.color;
            var lap = data.data.lapTime.lap + 1;
            var laptime = data.data.lapTime.millis/1000; // lap time in seconds
            console.log(racer + '(' + color + ') finished lap ' + lap + ' in ' + laptime + ' seconds!');
        }
        else if (data.msgType === 'crash') {

            /*  console.log(data.data.name + ' (' + data.data.color + ') has crashed!');

             console.log(data.data.name + ' (' + data.data.color + ') has crashed! (GameTick ' + data.gameTick + ')');
             >	  // lower throttle by 0.001
             throttle = throttle - throttleReduction;
             console.log('Throttle lowered by ' + throttleReduction + '. Current throttle ' + throttle);*/
            console.log('--*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*--------------');
            console.log('BOOM BIUM KABOOMMMM (car crashed..) angle was ' + carAngle );
            console.log('----**-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*----------');
        }
        else if (data.msgType === 'spawn') {
            console.log(data.data.name + ' (' + data.data.color + ') has spawned! (GameTick ' + data.gameTick + ')');
        }
        else if (data.msgType === 'finish') {
            console.log(data.data.name + ' (' + data.data.color + ') finished the ' + gameInit.data.race.track.name + ' Race!');
        }

        send({
            msgType: "ping",
            data: {}
        });
    }
});

jsonStream.on('error', function() {
    return console.log("disconnected");
});

function ticksToSeconds(ticks) {
    return ticks/60;
}

function trackPiece(piece) {
    var string = '';

    if (piece.length) {
        string += 'length ' + piece.length + ' ';
    }
    if (piece.switch) {
        string += 'switch ' + piece.switch + ' ';
    }
    if (piece.radius) {
        string += 'radius ' + piece.radius + ' ';
    }
    if (piece.angle) {
        string += 'angle ' + piece.angle + ' ';
    }
    return string;
}