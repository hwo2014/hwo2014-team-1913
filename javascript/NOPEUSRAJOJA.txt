

ULKOLANELLA!
7.5 - sopiva nopeus jolla selviää kaatumatta 2 peräkkäistä 45 asteen mutkaa (200 radius)
7.26 - menee 4 peräkkäistä 45 asteen mutkaa (200 radius)

(kestää 47,5 tickiä ajaa 180asteen mutka)

SISÄLANELLA!

6.98 - sopiva nopeus jolla selviää kaatumatta 2 peräkkäistä 45 asteen mutkaa (200 radius)
6.56 - menee 4 peräkkäistä 45 asteen mutkaa (200 radius)

(kestää 43 tickiä ajaa 180 asteen mutka tätä kautta)

-- TOIMIVAT NOPEUDET KEIMOLASSA --

//tilannenopeudet
var speed_straight = 10;
var speed_double45corner = 6.56;
var speed_single45corner = 7.265; //7.2 ajettava ainakin keimolassa //7.265 lentää radalta eka kierroksella mutta saa kovan ajan toisella..
var speed_22corner = 9.2; //7.5 toimiva //8.5 oli vielä ajettava ainakin keimolassa
var speed_Scorner = 8.0; //työn alla

