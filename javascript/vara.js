var net = require("net");
var JSONStream = require('JSONStream');

var serverHost = process.argv[2];
var serverPort = process.argv[3];
var botName = process.argv[4];
var botKey = process.argv[5];

console.log("I'm", botName, "and connect to", serverHost + ":" + serverPort);

// init default values
var defaultThrottle = 0.656; // trying to find nice default speed
var throttle = defaultThrottle;
var throttleReduction = 0.001; // how much throttle reduced after crash


var slowdownFactor = 0.01;

var pieces = [];

var oldPosInPiece = 0;

client = net.connect(serverPort, serverHost, function() {
    return send({
        msgType: "join",
        data: {
            name: botName,
            key: botKey
        }
    });
});

function send(json) {
    client.write(JSON.stringify(json));
    return client.write('\n');
};

jsonStream = client.pipe(JSONStream.parse());

jsonStream.on('data', function(data) {
    if (data.msgType === 'carPositions') {

        var pos = data.data[0].piecePosition.pieceIndex +1;

        //if (pieces.length > 0) {

        var speed = data.data[0].piecePosition.inPieceDistance - oldPosInPiece

        oldPosInPiece = data.data[0].piecePosition.inPieceDistance;

        var nextPos;

        if (pos < pieces.length) {
            nextPos = pos +1
        } else {
            nextPos = 0;
        }

        //check angle from current and next piece
        if (pieces[pos].angle != undefined && pieces[nextPos].angle != undefined) {

            //kalkulaattoreiden nopeus tieteellisellä algoritmillä

            var totalAngle = pieces[pos].angle + pieces[nextPos].angle;

            if (totalAngle < 0 ) totalAngle = -totalAngle;

            var rutle = 1 - totalAngle * slowdownFactor / 0.80;

            send({
                msgType: "throttle",
                data: rutle
            });

        } else {

            send({
                msgType: "throttle",
                data: 1
            });
        }
        // }
        /*
         send({
         msgType: "throttle",
         data: 0.5
         });*/

        // print time every 0.25 second
        if (data.gameTick % (15 * 1) == 0) {
            console.log((data.gameTick/60) + ' seconds has passed. Throttle ' + throttle + ' (GameTick ' + data.gameTick + ')');
            console.log('Car is on piece #' + pos);
            console.log('speed is ' + speed);
            console.log('next piece has angle of ' + pieces[pos].angle);
        }
    } else {
        if (data.msgType === 'join') {
            console.log('Joined')
        } else if (data.msgType === 'gameStart') {
            console.log('Race started');
            console.log('Default throttle ' + throttle);
        } else if (data.msgType === 'gameEnd') {
            console.log('Race ended');
        }
        else if (data.msgType === 'gameInit')
        {
            console.log('');
            console.log('-------------------------------');
            console.log('Game init');
            console.log('Track name is ' + data.data.race.track.name);
            console.log('It has x lanes and it is y distance long.');
            console.log('x cars are racing (1st x, 2nd y, 3rd z, 4th a)');
            console.log('Laps: ' + data.data.race.raceSession.laps + ' MaxTime: ' + data.data.race.raceSession.maxLapTimeMs + ' quickRace: ' + data.data.race.raceSession.quickRace);
            console.log('-------------------------------');
            console.log('');

            pieces = data.data.race.track.pieces;

            //console.log('tammonen rata ' + data.data.race.track.pieces);

        }
        else if (data.msgType === 'yourCar') {
            console.log('Your car \'' + data.data.name + '\' is ' + data.data.color + '.');
        }
        else if (data.msgType === 'lapFinished') {
            var laptime = data.data.lapTime.millis/1000; // lap time in seconds
            console.log('Lap finished in ' + laptime + ' seconds!');
        }
        else if (data.msgType === 'crash') {
            /*  console.log(data.data.name + ' (' + data.data.color + ') has crashed!');
             // lower throttle by 0.001
             throttle = throttle - throttleReduction;
             console.log('Throttle lowered by ' + throttleReduction + '. Current throttle ' + throttle);*/
            console.log('BOOM BIUM KABOOMMMM (car crashed..)');
        }
        else if (data.msgType === 'spawn') {
            console.log(data.data.name + ' (' + data.data.color + ') has spawned!');
        }
        else if (data.msgType === 'finish') {
            console.log(data.data.name + ' (' + data.data.color + ') finished the race in ' + (data.data.gameTick/60) + ' seconds!');
        }

        send({
            msgType: "ping",
            data: {}
        });
    }
});

jsonStream.on('error', function() {
    return console.log("disconnected");
});

function ticksToSeconds(ticks) {
    return ticks/60;
}